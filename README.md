uiscoped-bean-discovery-problem
==============

Sample application for demonstrating Vaadin `@UIScoped` bean discovery problem in multi-module projects.

Uses Vaadin 8.3.3, Java EE 7 and Vaadin CDI add-on. Requires a Java EE
application server for running, tested with WildFly 10.1, but should work
equally well with other Java EE 7 application servers.

See merge request #1 for the problem case.

Project Structure
=================

The project consists of the following modules:

- parent project: common metadata and configuration
- uiscoped-bean-discovery-problem-ui: main application module and UI
- uiscoped-bean-discovery-problem-backend: backend module, contains server side java code and dependencies

Workflow
========

To compile the entire project, run "mvn package" in the parent project.

Deploy the resulting WAR file into the application server and open
http://localhost:8080/uiscoped-bean-discovery-problem-ui-1.0.0/ to test.
