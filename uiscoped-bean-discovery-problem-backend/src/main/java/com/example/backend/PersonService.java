package com.example.backend;

import com.vaadin.cdi.UIScoped;

import javax.enterprise.context.SessionScoped;

@UIScoped
public class PersonService extends CrudService<Person> {
}
